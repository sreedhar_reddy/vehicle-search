I have developed & tested the program in Ubuntu environment .
1) Install casablanca , mongodb & dependenies .
2) copy the Source code "VehicleSearch" to /usr/casablanca/Release/samples/.
3) Modify the CMakeLists.txt in /usr/casablanca/Release/samples  to include VehicalSearch. 

    add_subdirectory(VehicleSearch)

    add_custom_target(samples
      DEPENDS SearchFile BingRequest blackjackclient blackjackserver oauth1client oauth2client Bank VehicleSearch
      )
4)Then complie the code as mentioned in the below link.
http://casablanca.codeplex.com/wikipage?title=Setup%20and%20Build%20on%20Linux&referringTitle=Documentation

    cd casablanca/Release
    mkdir build.release
    cd build.release
    CXX=g++-4.8 cmake .. -DCMAKE_BUILD_TYPE=Release
    make

5) open a putty shell to run server .
     
     eg:
     vagrant@ubuntu1404-i386:/usr/casablanca/Release/build.release$ ./Binaries/SearchServer
6) open another shell to run client.
     vagrant@ubuntu1404-i386:/usr/casablanca/Release/build.release$ ./Binaries/searchclient
7) At his point database is empty.
 lets insert test data using mongodb shell . Test data is present in  \Source\VehicleSearch\Mongo_db_Vehicle_test_data.txt.
 
 8) Search for the data by  entering One or More fileds .
    ex:
    Enter 
    Vehicle_Registration : KA01MM1234.    
    {  "Vehicle_Registration" : "KA01MM1234", "Vehicle_Make" : "2012", "Vehicle_Model" : "HONDA JAZZ", "Vehicle_Owner" : "SREEDHAR"  },
    
    All the matching results will be displayed.
    
9) Unit test   :
    Please run below application to unit test the Search  function.  
    vagrant@ubuntu1404-i386:/usr/casablanca/Release/build.release$ ./Binaries/VehicleSearchtest