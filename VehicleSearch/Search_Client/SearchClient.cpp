/***
*
* ==--==
* =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
*
* SearchClient.cpp : Defines the entry point for the console application
*
* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
****/

#include "stdafx.h"
#include <iostream>
#include <streambuf>
#include <sstream>
#include <fstream>
#include <algorithm> 
#include "../Search_Server/messagetypes.h"

#ifdef _WIN32
# define iequals(x, y) (_stricmp((x), (y))==0)
#else
# define iequals(x, y) boost::iequals((x), (y))
#endif

using namespace std;
using namespace web; 
using namespace utility;
using namespace http;
using namespace http::client;

http_response CheckResponse(const std::string &url, const http_response &response)
{
    //ucout << response.to_string() << endl;
    return response;
}


// 
// 
// Arguments: searchclient.exe <port>
// If port is not specified, client will assume that the server is listening on port 34568
//

int main(int argc, char *argv[])
{
    utility::string_t port = U("34568");
    if(argc == 2)
    {
        port = argv[1];
    }
    /*http_client_config config;
    credentials cred(L"username", L"Password");
    config.set_credentials(cred);*/
    utility::string_t address = U("http://localhost:");
    address.append(port);

    http::uri uri = http::uri(address);

    http_client client(http::uri_builder(uri).to_uri()/*,config*/);

    
    std::string input_data = "";
    while (true)
    {
      
        ucout<<"===============Welcome to National Stolen Vehicles Database============"<<endl;
        ucout <<"Please enter Vehicle details you would like to search"<<endl;
        ucout<<"Enter one or more details "<<endl <<"If you dont know some details Just press enter  "<<endl;
        ucout<<"================================================================"<<endl;
        // Create a JSON object.
        json::value obj;
        ucout <<"Vehicle_Registration :";
        getline(cin,input_data);
        transform(input_data.begin(), input_data.end(), input_data.begin(), ::toupper);
        obj["Vehicle_Registration"] = json::value::string(input_data);
        ucout <<"Vehicle_Make :";
        input_data = "";
        getline(cin,input_data);
        transform(input_data.begin(), input_data.end(), input_data.begin(), ::toupper);
        obj["Vehicle_Make"] = json::value::string(input_data);
        ucout <<"Vehicle_Model :";
        input_data = "";
        getline(cin,input_data);
        transform(input_data.begin(), input_data.end(), input_data.begin(), ::toupper);
        obj["Vehicle_Model"] = json::value::string(input_data);
        ucout <<"Vehicle_Owner :";
        input_data = "";
        getline(cin,input_data);
        transform(input_data.begin(), input_data.end(), input_data.begin(), ::toupper);
        obj["Vehicle_Owner"] = json::value::string(input_data);
        
        
        http_response response = CheckResponse("/search/dealer",client.request(methods::POST,"/search/dealer", obj).get());
        
        json::value  answer = response.extract_json().get();
        for(auto iter = answer.as_object().cbegin(); iter != answer.as_object().cend(); ++iter)
        {
            // Make sure to get the value as const reference otherwise you will end up copying 
            // the whole JSON value recursively which can be expensive if it is a nested object. 
            const utility::string_t &str = iter->first;
            const json::value &v = iter->second;

            // Perform actions here to process each string and value in the JSON object...
            ucout << "====================== Search resluts ==================== " <<endl<<  v.as_string() << endl;
            ucout << "=========================END============================== " <<endl;
        }
        
      
    }

    return 0;
}

