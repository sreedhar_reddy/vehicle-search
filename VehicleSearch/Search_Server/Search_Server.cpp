/***
* *
* ==--==
* =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
*
* File: Search_Servr.cpp 
* Author: Sreedhar
* Purpose:- Simple server application for Vechicle search ,This module is the entry point.
* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
****/

using namespace std;

#include "stdafx.h"
#include "SearchManager.h"



using namespace web;
using namespace http;
using namespace utility;
using namespace http::experimental::listener;

#include "Dealer.h"

std::unique_ptr<Dealer> g_httpDealer;

void on_initialize(const string_t& address)
{
    // Build our listener's URI from the configured address and the hard-coded path "bank/dealer"

    uri_builder uri(address);
    uri.append_path(U("search/dealer"));

    auto addr = uri.to_uri().to_string();
    
    
    g_httpDealer = std::unique_ptr<Dealer>(new Dealer(addr));
    g_httpDealer->open().wait();
    
    ucout << utility::string_t(U("Listening for requests at: ")) << addr << std::endl;

    return;
}

void on_shutdown()
{
    g_httpDealer->close().wait();
    return;
}

//
// To start the server, run the below command with admin privileges:
// Bank_Server.exe <port>
// If port is not specified, will listen on 34568
//
int main(int argc, char *argv[])
{
    utility::string_t port = U("34568");
    if(argc == 2)
    {
        port = argv[1];
    }
    std::cout << "############# National Stolen Vehicles Server .##########################" << std::endl;
    utility::string_t address = U("http://localhost:");
    address.append(port);

    on_initialize(address);
    std::cout << "Press ENTER to exit." << std::endl;

    std::string line;
    std::getline(std::cin, line);

    on_shutdown();
    return 0;
}