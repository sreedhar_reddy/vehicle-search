/*
 * File:   SearchManager.cpp
 * Author: Sreedhar
 *Purpose: It is a  module to Manage  the search functionality 
 * 
 */


class SearchManager
{

    public:
        SearchManager();
        string FindVehicle(string Vehicle_Registration,string Vehicle_Make,string Vehicle_Model,string Vehicle_Owner);
        
};

