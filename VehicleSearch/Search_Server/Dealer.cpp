/***

* =+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
*
* Dealer.cpp : Contains the main logic of the rest handling GET,PUT,POST & DELETE
*
* =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
****/

#include "stdafx.h"
#include "messagetypes.h"


using namespace std;
using namespace web; 
using namespace utility;
using namespace http;
using namespace web::http::experimental::listener;

#include <cpprest/json.h>
#include "SearchManager.h"
#include "Dealer.h"

#define TRACE(msg)            wcout << msg



Dealer::Dealer(utility::string_t url) : m_listener(url)
{
    mgr = new SearchManager();
    m_listener.support(methods::GET, std::bind(&Dealer::handle_get, this, std::placeholders::_1));
    m_listener.support(methods::PUT, std::bind(&Dealer::handle_put, this, std::placeholders::_1));
    m_listener.support(methods::POST, std::bind(&Dealer::handle_post, this, std::placeholders::_1));
    m_listener.support(methods::DEL, std::bind(&Dealer::handle_delete, this, std::placeholders::_1));
    
}

//
// A GET of the dealer resource produces a list of existing tables.
// 
void Dealer::handle_get(http_request message)
{
    
    ucout <<  message.to_string() << endl;

   TRACE(L"\nhandle GET\n");
 
    message.reply(status_codes::OK);
};

//
// A POST of the dealer resource creates a new table and returns a resource for
// that table.
// 
void Dealer::handle_post(http_request message)
{
      TRACE("\n handle POST\n");
    
    
    json::value answer;
    char *temp;
    
   message
      .extract_json()
      .then([this,&answer](pplx::task<json::value> task) {
         try
         {
            json::value  obj = task.get();
            string Vehicle_Registration;
            string Vehicle_Make;
            string Vehicle_Model;
            string Vehicle_Owner;
            string retval ;

            if (!obj.is_null())
            {
                
                for(auto iter = obj.as_object().cbegin(); iter != obj.as_object().cend(); ++iter)
                {
                    // Make sure to get the value as const reference otherwise you will end up copying 
                    // the whole JSON value recursively which can be expensive if it is a nested object. 
                    const utility::string_t &str = iter->first;
                    const json::value &v = iter->second;
                    if(str == "Vehicle_Registration")
                    {
                        Vehicle_Registration = v.as_string();
                        this->removeCharsFromString( Vehicle_Registration, "\"" );
                    }
                    else if(str == "Vehicle_Make")
                    {
                        Vehicle_Make = v.as_string();
                        this->removeCharsFromString( Vehicle_Make, "\"" );
                    }                        
                    else if(str == "Vehicle_Model")
                    {
                        Vehicle_Model = v.as_string();
                        this->removeCharsFromString( Vehicle_Model, "\"" );
                    }
                    else if(str == "Vehicle_Owner")
                    {
                        Vehicle_Owner = v.as_string();
                        this->removeCharsFromString( Vehicle_Owner, "\"" );
                    }
                    // Perform actions here to process each string and value in the JSON object...
                    //ucout << "String: " << str << ", Value: " << v.as_string() << endl;
                    
                }
                
                retval = mgr->FindVehicle(Vehicle_Registration,Vehicle_Make,Vehicle_Model,Vehicle_Owner);
                answer["result"] = json::value::string(retval);
                
                              
            }    
                
            
         }
         catch (http_exception const & e)
         {
            wcout << e.what() << endl;
         }
      })
      .wait();

   message.reply(status_codes::OK, answer);
   
};

//
// A DELETE of the player resource leaves the table.
// 
void Dealer::handle_delete(http_request message)
{
       TRACE("\nhandle DEL\n");

   
};


//
// A PUT to a table resource makes a card request (hit / stay).
// 
void Dealer::handle_put(http_request message)
{
    TRACE("\nhandle PUT\n");

   
};

 void Dealer::removeCharsFromString( string &str, char* charsToRemove ) {
   for ( unsigned int i = 0; i < strlen(charsToRemove); ++i ) {
      str.erase( remove(str.begin(), str.end(), charsToRemove[i]), str.end() );
   }
}


