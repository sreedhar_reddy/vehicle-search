/*
 * File:   SearchManager.cpp
 * Author: Sreedhar
 *Purpose: It is a  module to Manage  the search functionality 
 * 
 */
 
using namespace std;

#include <iostream>
#include <vector>
#include "DatabaseWrapper.h"
#include "SearchManager.h"

static DatabaseWrapper *db;

/**
   
    creata a DB if not present with the given name
    
    @param NA
    @return NA
    
**/

SearchManager::SearchManager()
{
    db = db->openDatabase("mydb");
    
}

/**
   
    @FindVehicle and log a transwaction in transactions table 
    @param vehicle details.
    @return json string with the result
    
**/
string SearchManager::FindVehicle(string Vehicle_Registration,string Vehicle_Make,string Vehicle_Model,string Vehicle_Owner)
{
    Transcations trans;
    Vehicle v = {Vehicle_Registration,Vehicle_Make,Vehicle_Model,Vehicle_Owner};
    trans.SearchQuery = "Vehicle_Registration :" + Vehicle_Registration + ", Vehicle_Make :" + Vehicle_Make + ", Vehicle_Model :" +Vehicle_Model +", Vehicle_Owner :"+ Vehicle_Owner;
    
    db->Create(TBL_TRANSACTION,&trans);
        
    return db->Read(TBL_VEHICLE,&v);
    
}


