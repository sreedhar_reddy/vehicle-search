/*
 * File:   DatabaseWrapper.h
 * Author: Sreedhar
 *Purpose: It is a wrapper module to execute CRUD on Mongo db  
 * 
 */

using namespace std;
#include <string>
#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>

struct Vehicle
{
    string reg_number;
    string mfg_year;
    string model;
    string owner;
};    
struct Transcations
{
    string SearchQuery;
};
#define TBL_VEHICLE      1
#define TBL_TRANSACTION  2

class DatabaseWrapper
{
    private:
        DatabaseWrapper(char* dbname); // Private so that it can  not be called
        DatabaseWrapper(DatabaseWrapper const&){}; // copy constructor is private
        DatabaseWrapper& operator=(DatabaseWrapper const&){};// assignment operator is private
        static DatabaseWrapper* m_pInstance;
        mongocxx::instance *inst;
        mongocxx::client *conn;
        mongocxx::database db;
    public:
        
        static DatabaseWrapper* openDatabase(char * dbname);
        bool Create(int tbl_id,void* data);
        string Read(int tbl_id,void *data);
        bool Update(int tbl_id,void *data);
        bool Delete(int tbl_id,void *data);
};