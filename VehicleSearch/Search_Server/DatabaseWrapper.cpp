/*
 * File:   DatabaseWrapper.cpp
 * Author: Sreedhar
 *Purpose: It is a wrapper module to execute CRUD on Mongo db  
 * 
 */

#include <iostream>
#include<cctype>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/json.hpp>

#include <mongocxx/client.hpp>
#include <mongocxx/instance.hpp>
#include "DatabaseWrapper.h"


#define IS_DATA_VALID(r) ((r.length()>0)&&isalnum(r[0]))

// Global static pointer used to ensure a single instance of the class.
DatabaseWrapper* DatabaseWrapper::m_pInstance = NULL; 

/**
   Private constructor for DatabaseWrapper
   Creata instance & open db connection

    @param dbname - Database name in mongo db.
    @return NA
    
**/
DatabaseWrapper::DatabaseWrapper(char * dbname)
{
    inst = new mongocxx::instance{};
    conn = new mongocxx::client{mongocxx::uri{}};
    db =  conn->database(dbname);
}

/**
    @openDatabase is static function to create a Singleton object of DatabaseWrapper
    @param dbname - Database name in mongo db.
    @return DatabaseWrapper object
    NOTE: for multi threaded application have a mutex while creating object    
**/

DatabaseWrapper* DatabaseWrapper::openDatabase(char * dbname)
{
 
    if(!m_pInstance)
        m_pInstance = new DatabaseWrapper(dbname);
    
    return m_pInstance;    
}
/**
    @Create to create a record in DB
    @param table id  & data.
    @return sucess/failure
      
**/

bool DatabaseWrapper::Create(int tbl_id,void* data)
{
    bool retval = false;
    if(tbl_id == TBL_VEHICLE)
    {
       Vehicle *v =  (Vehicle*)data;
       auto collection = db["Vehicle"];
       
       bsoncxx::builder::stream::document document{};           
       document <<"Vehicle_Registration" << v->reg_number
                <<"Vehicle_Make"<<v->mfg_year
                <<"Vehicle_Model"<<v->model
                <<"Vehicle_Owner"<<v->owner;

       collection.insert_one(document.view());
       retval = true;
    }
    else if(tbl_id == TBL_TRANSACTION)
    {
        Transcations *t = (Transcations*)data;
        auto collection = db["Transcations"];
        bsoncxx::builder::stream::document document{};
        
        document <<"Query"<<t->SearchQuery;
        collection.insert_one(document.view());
        retval = true;
    }
    return retval;
}


/**
    @Read to Query the DB for a given filter
    @param table id  & filter.
    @return json string with the results
      
**/

string DatabaseWrapper::Read(int tbl_id,void *filter)
{
    string retval= "";
    bool proceed = false;
    cout<<"Begin Read()"<<endl;
    if(tbl_id == TBL_VEHICLE)
    {
       Vehicle *v =  (Vehicle*)filter;
       auto collection = db["Vehicle"];
       bsoncxx::builder::stream::document document{};
       
        if(IS_DATA_VALID(v->reg_number))
        {
            document <<"Vehicle_Registration" << v->reg_number;
            proceed = true;        
        }
        if(IS_DATA_VALID(v->model))
        {
            document <<"Vehicle_Model"<<v->model;
            proceed = true;        
        }
        if(IS_DATA_VALID(v->mfg_year))
        {
            document <<"Vehicle_Make"<<v->mfg_year;
            proceed = true;
        }
        if(IS_DATA_VALID(v->owner))
        {
            document <<"Vehicle_Owner"<<v->owner;
           proceed = true;        
        }
        
        if(proceed)
        {
            std::cout <<"SEARCH FILTER IS : " <<bsoncxx::to_json(document) << std::endl;
            /*Use the below code to retrive all recods matching the filter */
            auto cursor = collection.find(document.view());

            for (auto&& doc : cursor) {
                std::cout << bsoncxx::to_json(doc) << std::endl;
                retval += bsoncxx::to_json(doc);
            }
           
            /*Use the below code to retrive only one  recod matching the filter */
            
            /*   mongocxx::stdx::optional<bsoncxx::document::value> maybe_result =
              collection.find_one(document.view());
            if(maybe_result) {
              std::cout << bsoncxx::to_json(*maybe_result) << "\n";
            } 
            */
        }
    }
    cout<<"End Read() retval = "<<retval<<endl;
    return retval;
}


/* TBD
bool DatabaseWrapper::Update(int tbl_id,void *data);
bool DatabaseWrapper::Delete(int tbl_id,void *data);
*/